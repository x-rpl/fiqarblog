<?php

session_start();
if(!isset($_SESSION["username"])) {
    header("location: home.php?error=silahkan login terlebih dahulu");
}

include 'koneksi.php';

$id = $_GET["id"];

if(!empty($id)) {
    $sql = "DELETE FROM users WHERE id=" . $id; 
    //jalanin mysql delete data
    if (mysqli_query($koneksi, $sql)) {
        header("Location: home.php?success=Record berhasil dihapus");
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($koneksi);
    }

    mysqli_close($koneksi);
} else {
    header("Location: home.php?error=Record tidak ditemukan");
}

?>