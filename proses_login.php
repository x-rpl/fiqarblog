<?php

session_start();
if(!isset($_SESSION["username"])) {
    header("location: home.php?error=silahkan login terlebih dahulu");
}

include 'koneksi.php';

$username = $_POST["username"];
$password = password_hash($_POST["password"],PASSWORD_BCRYPT, ["salt" => "this is my super duper secret "]);

$sql = "SELECT * FROM users WHERE username='$username' LIMIT 1";
$result = mysqli_query($koneksi,$sql);
$row = mysqli_fetch_assoc($result);

if($row != null) {
    if ($password == $row['password']){
        session_start();
        $_SESSION["uid"] = $row['id'];
        $_SESSION["username"] = $username;
        header("Location: home.php?success=Login berhasil");
    } else {
        header("Location: login.php?error=Username atau password salah");
    }
} else {
    header("Location: login.php?error=Username tidak terdaftar, silahkan register dahulu");
}

?>