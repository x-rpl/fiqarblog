<?php
session_start();
if(!isset($_SESSION["username"])) {
    header("location: login.php?error=Silahkan login terlebih dahulu");
}
unset($_SESSION["username"]);
unset($_SESSION["uid"]);

session_destroy();
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>
    <body style="background-color:#EFEFEF;">
        <div class="container mt-5 mb-5">
            <div class="card col-6 pl-0 pr-0 mx-auto">
                <img class="card-img-top" src="images/login_image.jpg" height="125px" alt="Card image cap">
                <div class="card-body">
                <div class="row">
                        <div class="col-12">
                            <h1>Berhasil logout</h1>
                            <p>Silahkan login kembali jika ingin melanjutkan.</p>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form action="login.php">
                                <button type="submit" class="btn btn-primary">login</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <small class="text-muted">silahkan <a href="register.php">register</a> jika belum punya akun</small>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>