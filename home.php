<?php

    include 'koneksi.php';
    
    session_start();

    if(!isset($_SESSION["username"])) {
        header("location: login.php?error=Silahkan login terlebih dahulu");
    }

    if(isset($_SESSION["uid"])) {
        $query = "select * from users";
        $result = mysqli_query($koneksi,$query);
        $rows = mysqli_fetch_all($result);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>Home</title>
    </head>
    <body>
        <?php
            if (isset($_GET['success'])) {
                echo '<div class="col alert alert-success alert-dismissible fade show" role="alert">' . $_GET["success"] . '
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>';
            } elseif (isset($_GET['error'])) {
                echo '<div class="row">
                        <div class="col alert alert-danger alert-dismissible fade show" role="alert">' . $_GET["error"] . '
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                    </div>';
            }
        ?>
        <div class="jumbotron mb-0" style="background-image: url(images/login_image.jpg); background-size: cover;">
            <center>
                <h1 class="text-light"><strong>Welcome To My Blog</strong></h1>
            </center>
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Post</a>
                    </li>
                </ul>
                <ul class="navbar-nav mr-sm-2">
                    <li class="nav-item">
                        <a class="nav-link text-warning" href="logout.php">Logout</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-12 mt-4">
                    <h4>Selamat Datang, <?php echo $_SESSION["username"];?>!</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-2">
                    <div class="card pl-0 pr-0">
                        <div class="card-header">
                            <h5>User Table</h5>
                            <button class='btn btn-success' data-toggle='modal' data-target='#addModal'><i class='fa fa-user-plus'></i></button>
                        </div>
                        <div class="card-body pl-0 pr-0 pb-0 pt-0 col-12">
                        <table class="table table-striped table-light mt-0 mb-0">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Id</th>
                                    <th>Full Name</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($rows as $row) {
                                        echo "<tr>";
                                        echo "<td id='dtId'>$row[0]</td>";
                                        echo "<td id='dtFullname'>$row[1]</td>";
                                        echo "<td id='dtUsername'>$row[2]</td>";
                                        echo "<td id='dtEmail'>$row[3]</td>";
                                        echo "<td><button class='btn btn-info ml-1 mr-1 edit-btn' data-toggle='modal' data-target='#editModal'><i class='fa fa-edit'></i></button>";
                                        echo "<a href='proses_hapus.php?id=$row[0]' class='btn btn-danger ml-1 mr-1' onclick='return confirm(\"Yakin hapus record ini?\");'><i class='fa fa-trash'></i></button></td>";
                                        echo "</tr>"; 
                                    }
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalLabel">Ubah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="proses_ubah.php" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <input type="hidden" class="form-control" id="uid" name="id" readonly>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fullname" class="col-sm-3 col-form-label">Full Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="fullname" name="fullname">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="username" class="col-sm-3 col-form-label">User Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="username" name="username">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 col-form-label">Email</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" id="email" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" id="password" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="confirmpass" class="col-sm-3 col-form-label">Confirm Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" id="confirmpass" name="confirm_password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addModalLabel">Tambah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="proses_tambah.php" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <label for="fullname" class="col-sm-3 col-form-label">Full Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="fullname" name="fullname">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="username" class="col-sm-3 col-form-label">User Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="username" name="username">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 col-form-label">Email</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" id="email" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" id="password" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="confirmpass" class="col-sm-3 col-form-label">Confirm Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" id="confirmpass" name="confirm_password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
                $('.edit-btn').click(function(){
                    var $data = $(this).closest('tr');
                    var $id = $data.find('#dtId').text();
                    var $fullname = $data.find('#dtFullname').text();
                    var $username = $data.find('#dtUsername').text();
                    var $email = $data.find('#dtEmail').text();
                    $('#uid').val($id);
                    $('#fullname').val($fullname);
                    $('#username').val($username);
                    $('#email').val($email);
                });
            });

            window.onload = function() {
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 4000);
            };
        </script>
    </body>
</html>