<!DOCTYPE html>
<html>
    <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>
    <body style="background-color:#EFEFEF;">
        <div class="container mt-5 mb-5">
            <div class="card col-8 pl-0 pr-0 mx-auto">
                <img class="card-img-top" src="images/register_image.jpg" height="125px" alt="Card image cap">
                <div class="card-body">
                    <?php
                        if (isset($_GET['success'])) {
                            echo '<div class="row">
                                    <div class="col alert alert-success alert-dismissible fade show" role="alert">' . $_GET["success"] . '
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                </div>';
                        } elseif (isset($_GET['error'])) {
                            echo '<div class="row">
                                    <div class="col alert alert-danger alert-dismissible fade show" role="alert">' . $_GET["error"] . '
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                </div>';
                        }
                    ?>
                    <div class="row">
                        <div class="col-12">
                            <h1>Register</h1>
                            <p>Tolong isi data ini untuk membuat akun.</p>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form action="proses_register.php" method="POST">
                                <div class="form-group row">
                                    <label for="fullname" class="col-sm-3 col-form-label"><b>Fullname</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Masukkan Fullname" name="fullname" id="fullname" required>
                                    </div>
                                </div>
                                <div class="form-group row">                            
                                    <label for="username" class="col-sm-3 col-form-label"><b>Username</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Masukkan Username" name="username" id="username" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label"><b>Email</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" placeholder="Masukkan Email" name="email" id="email" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="psw" class="col-sm-3 col-form-label"><b>Password</b></label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" placeholder="Masukkan Password" name="password" id="password" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="psw-repeat" class="col-sm-3 col-form-label"><b>Confirm Password</b></label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password" id="confirm_password" required>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script>
            window.onload = function() {
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 4000);
            };
        </script>
    </body>
</html>